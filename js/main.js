const nav = {
    characters: "https://rickandmortyapi.com/api/character",
    locations: "https://rickandmortyapi.com/api/location",
    episodes: "https://rickandmortyapi.com/api/episode"
};

const main$$ = document.querySelector('main');

function createA(text, url, menu) { //creamos el a, le metemos el innertext y añadimos las funciones
    const a$$ = document.createElement('a');
    a$$.innerHTML = text.toUpperCase();
    a$$.href = '#';
    a$$.addEventListener('click', function (e) { //Cuando hagamos click en a queremos que borre seccion y después haga un menu nuevo
        e.preventDefault();
        clearFirstChild('main'); //me elimina el primer hijo del elemento que le pase como parámetro
        menu(url); //me crea la section del menu que quiera
    })
    return a$$;
}

function header() { //esta funcion me crea el header con el nav y todos los a del nav con sus funciones
    const header$$ = document.createElement('header');
    const nav$$ = document.createElement('nav');
    const img$$ = document.createElement('img');
    img$$.src = "assets/img/logo.png";
    header$$.appendChild(img$$);
    for (property in nav) {
        const url = nav[property];
        const a$$ = createA(property, url, menuButtons);
        nav$$.appendChild(a$$);
    }
    header$$.appendChild(nav$$);
    document.body.insertBefore(header$$, main$$);
};

function clearFirstChild(element) { //esta funcion me elimina el primer hijo del elemento que le mande por parámetro
    const main$$ = document.querySelector(element);
    main$$.firstChild.remove();
};

function menuButtons(url) { //esta funcion me crea el menu de botones secundario

    fetch(url) //llamamos a la API
        .then((res) => {
            return res.json();
        })
        .then((res) => {
            //ahora queremos recorrer el array y sacar los nombres de cada pokemon
            const array = res.results;
            const section$$ = document.createElement('section');
            for (i = 0; i < array.length; i++) {
                const objeto = array[i];
                const name = objeto.name;
                const button$$ = document.createElement('button');
                button$$.innerHTML = name;
                button$$.addEventListener('click', function () {

                    const article$$ = document.createElement('article');
                    const section$$ = document.querySelector('section');

                    section$$.appendChild(article$$);

                    const h1$$ = document.createElement('h1');
                    h1$$.innerHTML = name;
                    article$$.appendChild(h1$$);

                    article$$.addEventListener('click', function () {
                        this.remove();
                    });

                    createImg(objeto); //si dentro del objeto hay una imagen, la pega en el articulo
                    ul$$ = createList(objeto);
                    article$$.appendChild(ul$$);

                })
                section$$.appendChild(button$$);
            }
            main$$.appendChild(section$$);
        });
}

function createImg(objeto) { //busca si dentro del objeto hay una imagen y la crea en el articulo
    article$$ = document.querySelector('article');

    for (property in objeto) {
        //console.log(property);
        //console.log(objeto[property]);
        if (property === 'image') {
            img$$ = document.createElement('img');
            img$$.src = objeto[property];
            article$$.appendChild(img$$);
        }
    }
}

function createList(objeto) {
    const ul$$ = document.createElement('ul');
    let count = 0;
    for (property in objeto) {
        if (property !== 'name' && property !== 'id' && objeto[property] !== '') {
            if (property == 'origin' || property == 'url') {
                count = 5;
            } else {
                count++;
                if (property === 'residents') {
                    const residents = property;
                    console.log(residents.length);
                } else if (property === 'characters') {
                    li$$ = document.createElement('li');
                    li$$.innerHTML = property + ': ';
                    ul$$.appendChild(li$$);
                    const arrayCar = objeto[property];

                    for (i = 0; i < arrayCar.length; i++) {
                        //console.log(arrayCar[i])
                        fetch(arrayCar[i]) //llamamos a la API
                            .then((res) => {
                                return res.json();
                            })
                            .then((res) => {
                                //console.log(res.name);
                                li$$ = document.createElement('li');
                                li$$.innerHTML = res.name;
                                li$$.classList.add('sublist');
                                ul$$.appendChild(li$$);

                            })
                    }
                } else {
                    li$$ = document.createElement('li');
                    li$$.innerHTML = property + ': ' + objeto[property];
                    ul$$.appendChild(li$$);
                }
            }
        };
        if (count > 3) {
            break;
        };
    }
    return ul$$;
}

window.onload = function () {
    header();
}